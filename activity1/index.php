<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03A: Classes and Objects</title>

</head>
<body>

	<h1>Person</h1>
    <?php $person->printName() ?>
    <h1>Developer</h1>
    <?php $developer->printName(); ?>
    <h1>Engineer</h1>
    <?php $engineer->printName(); ?>
</body>
</html>