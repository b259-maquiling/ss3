<?php

class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($first, $middle, $last) {
        $this->firstName = $first;
        $this->middleName = $middle;
        $this->lastName = $last;
    }

    public function printName() {
        echo "Your full name is $this->firstName $this->middleName $this->lastName";
    }
}

class Developer extends Person {
    public function printName() {
        echo "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
    }
}

class Engineer extends Person {
    public function printName() {
        echo "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
}


$person = new Person("Senku", "", "Ishigami");

$developer = new Developer("John", "Finch", "Smith"); 

$engineer = new Engineer("Harold", "Myers", "Reese");
