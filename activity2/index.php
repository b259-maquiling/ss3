<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03A: Classes and Objects</title>

</head>
<body>

	
    <h1>Building</h1>
    <p><?php echo $building->getName() . "<br/>" ?></p>
    <p>
    <?php echo "The " . $building->getName() . " has " . $building->getFloors() . " floors." . "<br/>"?>

    </p>
    <p>
    <?php echo "The " . $building->getName() . " is located at " . $building->getAddress() . "." . "<br/>" ?>

    </p>

    <p>
    <?php $building->setName("Caswynn Complex"). "<br/>"; ?>

    </p>
    <p>
        <?php echo $building->getName() ?>

    </p>

    <h1>Condominium</h1>
    <p>
        <?php echo $condo->getName() . "<br/>"?>

    </p>
    <p>
        <?php echo "The " . $condo->getName() . " has " . $condo->getFloors() . " floors." . "<br/>"?>

    </p>
    <p>
        <?php echo "The " . $condo->getName() . " is located at " . $condo->getAddress() . "." . "<br/>"?>

    </p>

    <p>

        <?php echo $condo->setName("Enzo Tower") ?>
    </p>
   
</body>
</html>