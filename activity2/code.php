<?php
class Building {
    protected $name;
    private $floors;
    private $address;

    function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName() {
        return "The " . $this->name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setName($name) {
        $this->name = $name;
        return "The name of the building has been changed to $name.";
    }

    public function setFloors($floors) {
        $this->floors = $floors;
    }

    public function setAddress($address) {
        $this->address = $address;
    }
}

class Condominium extends Building {
    public function setName($name) {
        $this->name = $name;
        return "The name of the condominium has been changed to $name.";
    }
}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");

$condo = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");