<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03B: Access Modifiers and Encapsulation</title>
</head>
<body>

	<h1>Access Modifiers</h1>

	<h2>Building Variable</h2>
	<p> <?php print_r($building) ?>  </p>
	<p> <?php // echo $building->name; ?> </p>


	<h3>Condominium Variable</h3>
	<p> <?php print_r($condominium) ?>  </p>
	<p> <?php //echo $condominium->name; ?> </p>



	<h1>Encapsulation</h1>

	<h3>Condominium Variable</h3>
	<p> <?php print_r($condominium) ?>  </p>

	<!-- GETTER -->
	<p> The name of the condominium is <?php echo $condominium->getName(); ?> </p>

	<!-- SETTER -->
	<?php $condominium->setName(50) ?>
	<p> The name of the condominium is <?php echo $condominium->getName(); ?> </p>

</body>
</html>